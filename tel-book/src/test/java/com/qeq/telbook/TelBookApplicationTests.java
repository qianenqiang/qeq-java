package com.qeq.telbook;

import com.alibaba.fastjson.JSON;
import com.qeq.telbook.entity.TelBook;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TelBookApplicationTests {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void contextLoads() {
        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from test.t_tel_book");
        System.out.println(JSON.toJSONString(maps));
    }
    @Test
    public void toObj() {
        TelBook telBook = jdbcTemplate.queryForObject("select * from test.t_tel_book", BeanPropertyRowMapper.newInstance(TelBook.class));
        logger.info(JSON.toJSONString(telBook));

    }
    @Test
    public void insertTest() {
        TelBook telBook = new TelBook();
        telBook.setName("xiaoming");
        telBook.setAddress("zhanghou");
        telBook.setAge(18);
        telBook.setBirth(new Date());
        telBook.setGender(true);
        telBook.setMobile(13333333333L);
        int insertCount = jdbcTemplate.update("INSERT INTO test.t_tel_book (name, age, mobile, address, birth, gender) VALUES (?, ?, ?, ?, ?, ?)", new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1, telBook.getName());
                preparedStatement.setInt(2, telBook.getAge());
                preparedStatement.setLong(3, telBook.getMobile());
                preparedStatement.setString(4, telBook.getAddress());
                preparedStatement.setDate(5, new java.sql.Date(telBook.getBirth().getTime()));
                preparedStatement.setBoolean(6, telBook.getGender());
            }
        });
        logger.info("insert success count :{}", insertCount);
        TelBook query = jdbcTemplate.queryForObject("select * from test.t_tel_book where name=?", new Object[]{telBook.getName()}, BeanPropertyRowMapper.newInstance(TelBook.class));

        logger.info(JSON.toJSONString(query));
    }
    //增
    //删
    //改
    //查单条
    //查全部
}
