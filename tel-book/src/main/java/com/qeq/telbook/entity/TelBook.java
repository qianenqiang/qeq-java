package com.qeq.telbook.entity;

import java.util.Date;

public class TelBook {
    private Long id;
    private String name;
    private Integer age;
    private Long mobile;
    private String address;
    private Date birth;
    private Boolean gender;

    public TelBook() {
    }

    public TelBook(Long id, String name, Integer age, Long mobile, String address, Date birth, Boolean gender) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.mobile = mobile;
        this.address = address;
        this.birth = birth;
        this.gender = gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }
}
