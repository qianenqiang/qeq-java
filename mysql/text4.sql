use study;
select * from emp;
-- 取得员工薪水的合计
select sum(sal) as sumsal from emp;
-- 取得总薪水
select sum(sal+ifnull(comm,0)) as totalsal from emp;
select sum(comm) as totalcomm from emp;
-- 取得平均薪水
select avg(sal) as avgsal from emp;
-- 取得最高薪水
select ename,max(sal) as maxsal from emp;
-- 取得最低薪水
select ename,min(sal) as minsal from emp;
-- 取得最高薪水
select sal from emp order by sal desc;
-- 取得最晚入职的日期
select max(hiredate) as maxhiredate from emp;
select * from emp;
-- 取得最晚入职日期
select min(hiredate) as minhiredata from emp;
-- 取得所有的员工数
select count(*) from emp;
-- 取得补助不为空的员工数
select count(comm) from emp;
-- 统计没有补助的员工数
select count(*) from emp where comm is null;
-- 组合复用函数
select count(*),sum(sal),min(sal),max(sal),min(sal) from emp;
