-- 以后查询，更新，插入，删除之类的sql在idea里面写
-- 类似那种创建数据库的在这里面记一下
-- 目录结构分清楚
show databases;
use qian1;
/*MySql注释*/
create table emp(
    id int,
    name varchar(100),
    sex char(6),
    birthday date,
    salary float(10,2)
);
-- 创建表
insert into emp(id,name,sex,birthday,salary) values (1,'andy','male','1995-05-15',10000);-- 添加数据
show tables;
insert into emp values (2,'paul','male','1996-06-15',15000);
insert into emp values (3,'Lucy','female','1998-07-11',8000);
-- 批量添加
insert into emp(id,name,sex,birthday,salary) values
(4,'james','male','1985-08-10',50000),
(5,'marry','female','1987-06-15',30000),
(6,'carter','male','1995-05-18',10000);
show tables;
select *from emp;
/*修改*/
-- 修改
-- 下面写法会将表中的数据全部修改，使用时一定注意
update emp set salary=3000;
-- 限定修改的数据，告诉mysql要修改的name='andy'的那条数据
update emp set salary=3000 where name='andy';
update emp set sex='male',salary=10000 where id='5';
-- 修改多个字段，使用逗号将字段隔开
/*删除*/
delete from emp where name='lucy';
delete from emp;
-- 将表中数据全部删除
truncate table emp;
show databases;
use mysql;
show tables;
use qian1;
show tables;
select * from emp;
create database study;








