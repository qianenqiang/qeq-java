use study;
show tables;
create table emp(
    empno    int(4),
    ename    varchar(10),
    job      varchar(9),
    mgr      int(4),
    hiredate date,
    sal      double(7, 2),
    comm     double(7, 2),
    deptno   int(2)
);
drop table emp;
insert into emp values    (7369,'smith','clerk',7902,'1980-12-17',800,null,20),
                          (7499,'allen','salesman',7698,'1981-02-20',1600,300,30),
                          (7521,'ward','salesman',7698,'1981-02-22',1250,500,30),
                          (7566,'jones','manager',7939,'1981-12-17',2975,null,20),
                          (7654,'martin','salesman',7698,'1981-09-28',1250,1400,30),
                          (7698,'wom','manager',7839,'1981-05-01',2850,null,30),
                          (7782,'wop','manager',7839,'1981-08-21',2450,null,10),
                          (7788,'jack','analyst',7566,'1987-04-22',3000,null,20),
                          (7839,'tom','president',null,'1981-06-15',5000,null,10),
                          (7844,'xia','salesman',7698,'1981-08-13',1500,0,30),
                          (7876,'pem','clerk',7788,'1987-07-18',1100,null,20),
                          (7900,'qom','clerk',7698,'1981-11-15',950,null,30),
                          (7902,'wws','analyst',7566,'1981-11-25',3000,null,20),
                          (7934,'zpl','clerk',7782,'1982-12-26',1300,null,10);
select * from emp;
-- 开发中不建议使用*号
select ename from emp;
select empno,ename from emp;
select sal from emp where sal*12;
-- 使用as给字段取别名
select empno,ename,sal*12 as yearsal from emp;
select empno,ename,sal*12 yearsal from emp;
select empno,ename,sal*12 as '年薪' from emp;
-- 查询薪水是5000的员工
select ename,empno from emp where sal=5000;
-- 查询job是manager的员工
select empno,ename from emp where job='manager';
-- 查询薪水不等于5000的员工
-- 建议使用<>
 select empno,ename from emp where sal !=5000;
select empno,ename from emp where sal <> 5000;
-- 查询薪水1600-3000的员工
select empno,ename from emp where sal>=1600 and sal<=3000
select empno,ename from emp where sal between 1600 and 3000;
-- 查询津贴为空的员工
select empno,ename,comm from emp where comm is null;
-- 查询工作岗位为manager并且薪水大于2500的员工
select empno,ename,job,sal from emp where job='manager' and sal>2500;
-- 查询出job是salesman 或job为manager的员工
select empno,ename,job from emp where job='salesman' or job='manager';
-- and优先级大于or
-- 查询薪水大于1800，并且部门编号为20或30的员工
select empno,ename,deptno,sal from emp where sal>1800 and (deptno=20 or deptno=30);
-- 查询出job是salesman或者是manager的员工
select empno,ename from emp where job in ('salesman','manager');
-- 查询出薪水不是1600和3000的员工
select empno,ename from emp where sal not in(1600,3000);
-- 查询姓名以m开头的员工
select ename from emp where ename like 'm%';
-- 查询姓名以n结尾的员工
select ename from emp where ename like '%n';
-- 查询姓名中包含o的员工
select ename from emp where ename like '%o%';
-- 查询姓名第二个字母是n的员工
select ename from emp where ename like '_a%';
-- 查询第二个字母是a的员工
select ename from emp where ename like '__a%';
