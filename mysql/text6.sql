use study;
-- 显示每个员工信息，并显示所属部门名称
select e.ename,d.dname from dept d,emp e;
-- sql92语法
select e.ename,d.dname from emp e,dept d where e.deptno=d.deptno;
select count(*) from emp;
select count(*) from dept;
-- sql99语法
select e.ename,d.dname from emp e join dept d on e.deptno=d.deptno;
-- 找出员工所对应的工资等级，显示员工姓名、工资等级
select * from salgrade;
select e.ename,e.sal,s.grade from emp e join salgrade s on e.sal between s.losal and s.hisal;
-- 查询员工的名称并且员工所对应的领导的名称
select a.ename as leader,b.ename from emp a join emp b on a.empno=b.mgr;
-- 找出每一个员工对应的部门名称，要求部门名称全部显示
select e.ename,d.dname from dept d left join emp e on e.deptno=d.deptno;
-- 找出每一个员工对应的领导名，要求显示所有的员工
select a.ename as leader,b.ename from emp a right join emp b on a.empno=b.mgr;
-- 找出每一个员工对应的部门名称以及该员工对应的工资等级，要求显示员工姓名、部门名称、工资等级
select e.ename,e.sal,d.dname from emp e join dept d on e.deptno=d.deptno
join salgrade as s on e.sal between  s.losal and s.hisal;