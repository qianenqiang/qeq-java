use study;
select ename from emp;
-- 找出薪水比平均薪水高的员工，要求显示员工名和薪水
select ename,sal from emp where sal>
(select avg(sal) as avgsal from emp);
-- 找出每个部门的平均薪水，并且要求显示平均薪水的薪水等级
select s.grade,t.job,t.avgsal from salgrade s join(select job,avg(sal) as avgsal from emp
    group by job) as t on t.avgsal between s.losal and s.hisal;
select * from salgrade;
-- 查询出job问manager和salesman的员工
select ename,job from emp where job='manager' or job='salesman';
select ename,job from emp where job in('manager','salesman');
-- 使用union关键字
select ename,job from emp where job='manager' union
select ename,job from emp where job='salesman';
-- 取前五个员工的信息 limit关键字
select * from emp limit 0,5;
-- 找出工资排在前五名的员工
select sal from emp sal order by sal limit 5;
-- 找出工资排在第【3-9】的员工
select sal from emp order by sal limit 2,7;
