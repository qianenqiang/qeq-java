-- 查询员工姓名，将员工姓名转换为小写
use study;
select lower(ename) from emp;
-- 查询员工姓名，将员工姓名转换为大写
select upper(ename) from emp;
-- 显示并显示所有员工姓名的第二个字母
select substr(ename,2,1) from emp;
-- 查询员工姓名中的第二个字母为A的所有员工
select ename from emp where substr(ename,2,1)='A';
select ename from emp where ename like'_a%';
-- 取的员工姓名长度
select ename,length(ename) as namelength from emp;
select * from emp;
-- 查询员工姓名及补助，如果补助为null设置为0
select ename,ifnull(comm,0) from emp;
-- 查询员工总薪水
select ename,(sal+comm) as total from emp;
select ename,ifnull(comm,0) as total from emp;
-- 如果没有补助的员工 每月补助100，求员工年薪
select ename,(sal+ifnull(comm,100)) * 12 as yearsal from emp;
-- 匹配工作岗位，当为MANAGE时，薪水上调10%，当为salesman时，薪水上调50%，其他岗位薪水不变
select ename,sal,job,(case job
when 'manager' then sal*1.1
when 'salesman' then sal*1.5
else sal
end) as newsal
from emp;
-- 取得岗位为manager的所有员工
select ename,job from emp where job=trim('  manager  ');
-- 查看员工薪水保留一位小数
select round(sal,1) as sal from emp;
select round(10.24,1);
-- rand()
select rand();
-- 生成多个随机数
select rand(),sal from emp;
-- 生成0—100之间的随机数
select round(rand()*100,0),sal from emp;
-- 查询1981-12-03入职的员工
select empno,ename from emp where hiredate='1981-12-03';
-- 使用str_to_date()函数查询02-20-1981年入职的员工
select empno,ename,hiredate from emp where hiredate=str_to_date('02-20-1981','%m-%d-%y');
-- 查询员工入职日期，以‘10-12-1980'的格式显示到窗口中
select date_format(hiredate,'%m-%d-%y') from emp;