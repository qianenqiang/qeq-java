use study;
-- 按照薪水由小到大排序
select sal,ename from emp order by sal;
-- 取得job为manager的员工 按从小到大排序
select ename,sal,job from emp where job='manager' order by sal;
-- 手动指定按照薪水由小到大排序
select ename,sal from emp order by sal asc;
-- 手动指定按照薪水由大到小排序
select ename,sal from emp order by sal desc;
-- 按照job和薪水倒叙排序
select ename,sal,job from emp order by job,sal desc;
-- 按照薪水升序排序，不建议使用，不明确
select * from emp order by 6;
create table dept(
    deptno int(2),
    dname varchar(14),
    loc varchar(13)
);
insert into dept values
                        (10,'accounting','new york'),
                        (20,'research','dallas'),
                        (30,'sales','chicago'),
                        (40,'operations','booston');
select * from dept;
create table salgrade(
    grade int (11),
    hisal int(11),
    losal int(11)
);
insert into salgrade values
                            (1,1200,700),
                            (2,1400,700),
                            (3,2000,1401),
                            (4,3000,2001),
                            (5,9999,3001);
select * from salgrade;

