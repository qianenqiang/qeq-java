use study;
-- 找出没选过“黎明”老师的所有学生姓名
-- 第一步 找出黎明老师所授课程的编号
    select cno from c where cteacher='黎明';
-- 用过sc表cno=第一步中结果的sno，这些sno就是选了黎明老师的学生编号
    select sno from sc
join
        (select cno from c where cteacher='黎明') t
on
sc.cno=t.con;
-- 将第二步中的sno去和s表中的sno做关联查询
    select s.sname from s
join
    ( select sno from sc
        join
         (select cno from c where cteacher='黎明') t
        on
            sc.cno=t.con) t1
on
   t.sno=t1.sno;
-- 列出2门以上（含2门）不及格学生姓名及平均成绩
-- 第一步，查询出两门以上不及格的学生编号
select sno from sc where scgrade<60 group by sno having count(*)>=2;
-- 第二步，将sc表与第一步中的结果作连接，计算平均成绩
select t2.sno,avg(t1.scgrade) as avggrade from sc t1
join
    (select sno from sc where scgrade<60 group by sno having count(*)>=2) t2
on
t1.sno=t2.sno
group by
t2.sno;
-- 将第二步中的查询结果与学生表做连接查找出学生姓名
select s.sname,t3.avggrade from s
join
    (select t2.sno,avg(t1.scgrade) as avggrade from sc t1
        join
        (select sno from sc where scgrade<60 group by sno having count(*)>=2) t2
        on
         t1.sno=t2.sno
             group by t2.sno) t3
on
t3.sno=s.sno;
-- 既学过1号课程又学过2号课程所有学生的姓名
-- 第一步找出学过1号课程的学生编号
select sno from sc where cno=1;
-- 第二步找出学过2号课程的学生编号
select sno from sc where cno=2;
-- 第三步找出第一步和第二步的交集数据，做连接查询
select t1.sno from
    (select sno from sc where cno=1) t1
join
    (select sno from sc where cno=2) t2
on
t1.sno=t2.sno;
-- 第四步，将第三步中的结果与s表做连接查询，找出学生姓名
select s.sname from s
join
    (select t1.sno from
     (select sno from sc where cno=1) t1
        join
     (select sno from sc where cno=2) t2
        on
            t1.sno=t2.sno) t3
on
s.sno=t3.sno;