use study;
show engines;
-- alter table 表明 engine= 存储引擎名称;
create table table_name(
    id int
)engine=InnoDB;
show create table emp;
show variables like '%commit%';
create table t_bank(
    account int(10) primary key,
    money int(15)
);
insert into t_bank values (1001,1000),(1002,2000);
show databases;
select * from t_bank;
start transaction;
update t_bank set money=500 where account=1001;
update t_bank set money=2500 where account=1002;
commit;
rollback;
select money from t_bank;
select @@tx_isolation;
create index dept_dname_index on dept(dname);
show index from dept;
drop index dept_dname_index on dept;
create view e_info as select empno,ename,sal from emp;
alter view e_info as select job,sal from emp;
select * from e_info;
drop view if exists e_info;
-- 数据库导入导出
-- mysqldump -u monkey1024 > d:init.sql
-- source d:/init.sql
select * from salgrade;
select empno,ename,job,mgr,hiredate,sal,comm,deptno from emp;
select * from emp;

