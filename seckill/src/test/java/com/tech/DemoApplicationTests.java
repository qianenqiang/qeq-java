package com.tech;

import com.alibaba.fastjson.JSON;
import com.tech.domain.mapper.UserEntityMapper;
import com.tech.domain.model.UserEntity;
import com.tech.rabbit.Publisher;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserEntityMapper userEntityMapper;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private Publisher publisher;

    @Test
    public void contextLoads() {
        Date date = jdbcTemplate.queryForObject("select sysdate()", Date.class);
        log.info("db:" + JSON.toJSONString(date));
        UserEntity userEntity = userEntityMapper.selectByPrimaryKey(1L);
        log.info("user:" + JSON.toJSONString(userEntity));
        redisTemplate.opsForValue().set("anzhen", "anzhen");
        log.info("redis:" + redisTemplate.opsForValue().get("anzhen"));
        publisher.sendToTestQueue("i am a robot");


    }


    @Test
    public void maLoadTest() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        for (int i = 0; i < 8; i++) {
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        publisher.sendToTestQueue("i am a robot");
                    }
                }
            });
        }
        while (!executorService.isTerminated()) {
            TimeUnit.SECONDS.sleep(1L);
        }
    }


}
