package com.tech.service.impl;

import com.tech.domain.mapper.GoodsEntityMapper;
import com.tech.domain.mapper.OrderEntityMapper;
import com.tech.domain.mapper.RepertoryEntityMapper;
import com.tech.domain.mapper.UserEntityMapper;
import com.tech.domain.model.GoodsEntity;
import com.tech.domain.model.OrderEntity;
import com.tech.domain.model.RepertoryEntity;
import com.tech.domain.model.RepertoryEntityExample;
import com.tech.domain.model.UserEntity;
import com.tech.service.SeckillService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Service
public class SeckillServiceImpl implements SeckillService {

    @Autowired
    private UserEntityMapper userEntityMapper;

    @Autowired
    private GoodsEntityMapper goodsEntityMapper;

    @Autowired
    private OrderEntityMapper orderEntityMapper;

    @Autowired
    private RepertoryEntityMapper repertoryEntityMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Long order(Long uId, Long gId) {
        //查用户
        UserEntity userEntity = userEntityMapper.selectByPrimaryKey(uId);
        if (userEntity == null) {
            throw new RuntimeException("用户不存在 id:" + uId);
        }
        //查商品
        GoodsEntity goodsEntity = goodsEntityMapper.selectByPrimaryKey(gId);
        if (goodsEntity == null) {
            throw new RuntimeException("商品不存在 id:" + gId);
        }
        if (userEntity.getBalance().compareTo(goodsEntity.getPrice()) < 0) {
            throw new RuntimeException("余额不足 id:" + uId);
        }
        //查库存
        RepertoryEntityExample repertoryEntityExample = new RepertoryEntityExample();
        repertoryEntityExample.createCriteria().andGIdEqualTo(gId).andRemainCountGreaterThan(0);

        List<RepertoryEntity> repertoryList = repertoryEntityMapper.selectByExample(repertoryEntityExample);
        if (repertoryList.isEmpty()) {
            throw new RuntimeException("库存不足 id:" + gId);
        }
        //减库存
        RepertoryEntity repertoryEntity = repertoryList.get(0);
        int subtract = repertoryEntityMapper.subtract(repertoryEntity.getId());
        if (subtract != 1) {
            throw new RuntimeException("库存不足 id:" + gId);
        }
        //下订单
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setgId(gId);
        orderEntity.setuId(uId);
        orderEntityMapper.insert(orderEntity);
        //减余额
        BigDecimal price = goodsEntity.getPrice();
        userEntity.setBalance(price);
        userEntity.setId(uId);
        int balance = userEntityMapper.balance(userEntity);
        if (balance != 1) {
            throw new RuntimeException("余额不足 id:" + uId);
        }
        return orderEntity.getgId();
    }

    @Override
    public OrderEntity queryOrder(Long id) {
        return null;
    }
}
