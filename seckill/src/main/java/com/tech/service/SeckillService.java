package com.tech.service;

import com.tech.domain.model.OrderEntity;

public interface SeckillService {

    Long order(Long uId, Long gId);

    OrderEntity queryOrder(Long id);

}
