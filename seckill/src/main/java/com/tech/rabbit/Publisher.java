package com.tech.rabbit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Publisher {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendToTestQueue(String message) {
        log.info("send message to test_queue message:{}", message);
        rabbitTemplate.convertAndSend("test_queue",message);
    }
}
