package com.tech.rabbit;


import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class Receiver {

    //@RabbitListener(queues = "test_queue")
    //public void testQueueReceiver(String message) {
    //    log.info("receive message from test_queue :{}", message);
    //}

    @RabbitListener(queues = "test_queue")
    public void testQueueReceiverAck(Message message, Channel channel) throws IOException {
        channel.basicReject(message.getMessageProperties().getDeliveryTag(), false);
        log.info("receive message from test_queue :{}", message);
    }
}
