package controller;

import Service.LoginService;
import Service.LoginServicenew;
import bean.User;

import java.util.Scanner;

public class Login {
    public static void main(String[] args){
        Scanner input=new Scanner(System.in);
        System.out.println("请输入用户名");
        String ename=input.nextLine();
        LoginServicenew ls=new LoginServicenew();
        User u=ls.findUserByNameAndPassword(ename);
        if(u==null){
            System.out.println("同户名或密码错误");
        }else{
            System.out.println("登陆成功");
        }
    }
}
