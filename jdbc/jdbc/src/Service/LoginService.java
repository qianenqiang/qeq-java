package Service;

import bean.User;
import jdbc.util.DBUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LoginService {
    public User findUserByNameAndPassword(String ename){
        User u=null;
        //获取连接Connection
        try (Connection conn = DBUtil.getConnection();
             //得到执行sql语句的对象Statement
             Statement stmt = conn.createStatement();
             //执行sql并返回结果
             ResultSet rs = stmt.executeQuery("select empno,ename,job,mgr,hiredate,sal,comm,deptno from emp where ename="+ename)) {
            //处理结果
            while (rs.next()) {
                u = new User();
                u.setEmpno(rs.getInt("empno"));
                u.setEname(rs.getString("ename"));
                u.setJob(rs.getString("job"));
                u.setMgr(rs.getInt("mgr"));
                u.setHiredate(rs.getDate("hiredate"));
                u.setSal(rs.getDouble("sal"));
                u.setComm(rs.getDouble("comm"));
                u.setDeptno(rs.getInt("deptno"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return u;
    }

}
