package Service;

import bean.User;
import jdbc.util.DBUtil;

import java.sql.*;

public class LoginServicenew {
    public User findUserByNameAndPassword(String ename) {
        User u = null;
        String sql = "select empno,ename,job,mgr,hiredate,sal,comm,deptno from emp where ename=?";
        //获取连接Connection
        try (Connection conn = DBUtil.getConnection();
             //得到执行sql语句的对象Statement
             PreparedStatement stmt = conn.prepareStatement(sql);

        ) {
            stmt.setString(1, ename);
            try (
                    //执行sql并返回结果;
                    ResultSet rs = stmt.executeQuery()
            ) { //处理结果
                while (rs.next()) {
                    u = new User();
                    u.setEmpno(rs.getInt("empno"));
                    u.setEname(rs.getString("ename"));
                    u.setJob(rs.getString("job"));
                    u.setMgr(rs.getInt("mgr"));
                    u.setHiredate(rs.getDate("hiredate"));
                    u.setSal(rs.getDouble("sal"));
                    u.setComm(rs.getDouble("comm"));
                    u.setDeptno(rs.getInt("deptno"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return u;
    }
}
