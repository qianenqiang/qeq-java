package bean;

import java.util.Date;

/**
 * 用户
 */
public class User {
    private int empno;
    private String ename;
    private String job;
    private int mgr;
    private Date hiredate;
    private Double sal;
    private Double comm;
    private int deptno;
    public int getEmpno() {
        return empno;
    }
    public Date getHiredate() {
        return hiredate;
    }

    public int getMgr() {
        return mgr;
    }

    public void setMgr(int mgr) {
        this.mgr = mgr;
    }

    public String getJob() {
        return job;
    }
    public String getEname() {
        return ename;
    }
    public void setJob(String job) {
        this.job = job;
    }
    public Double getComm() {
        return comm;
    }
    public void setEname(String ename) {
        this.ename = ename;
    }
    public void setEmpno(int empno) {
        this.empno = empno;
    }
    public int getDeptno() {
        return deptno;
    }
    public Double getSal() {
        return sal;
    }
    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }
    public void setComm(Double comm) {
        this.comm = comm;
    }
    public void setSal(Double sal) {
        this.sal = sal;
    }
    public void setHiredate(Date hiredate) {
        this.hiredate = hiredate;
    }

    @Override
    public String toString() {
        return "User{" +
                "empno=" + empno +
                ", ename='" + ename + '\'' +
                ", job='" + job + '\'' +
                ", mgr=" + mgr +
                ", hiredate=" + hiredate +
                ", sal=" + sal +
                ", comm=" + comm +
                ", deptno=" + deptno +
                '}';
    }
}