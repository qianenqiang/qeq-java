package jdbc;

import bean.User;
import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Crud_Test02 {
    @Test
    public void testSelect() throws SQLException {
        ResultSet rs1=null;
        try {
            //注册驱动
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //获取连接Connection
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/study", "root",
                "qianenqiang");
             //得到执行sql语句的对象Statement
             Statement stmt = conn.createStatement();
             //执行sql并返回结果
             ResultSet rs = stmt.executeQuery("select empno,ename,job,mgr,hiredate,sal,comm,deptno from emp")) {
            //处理结果
            List<User> userList = new ArrayList<>();
            while (rs.next()) {
                User u = new User();
                u.setEmpno(rs.getInt("empno"));
                u.setEname(rs.getString("ename"));
                u.setJob(rs.getString("job"));
                u.setMgr(rs.getInt("mgr"));
                u.setHiredate(rs.getDate("hiredate"));
                u.setSal(rs.getDouble("sal"));
                u.setComm(rs.getDouble("comm"));
                u.setDeptno(rs.getInt("deptno"));
                userList.add(u);
            }
            System.out.println(userList);
            rs1=rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            System.out.println(rs1.isClosed());
        }
        // 关闭Connection
    }
}
