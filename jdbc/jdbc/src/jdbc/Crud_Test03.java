package jdbc;

import bean.User;
import jdbc.util.DBUtil;
import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Crud_Test03 {
    @Test
    public void testSelect() throws Exception {
        //获取连接Connection
        try (Connection conn = DBUtil.getConnection();
             //得到执行sql语句的对象Statement
             Statement stmt = conn.createStatement();
             //执行sql并返回结果
             ResultSet rs = stmt.executeQuery("select empno,ename,job,mgr,hiredate,sal,comm,deptno from emp")) {
            //处理结果
            List<User> userList = new ArrayList<>();
            while (rs.next()) {
                User u = new User();
                u.setEmpno(rs.getInt("empno"));
                u.setEname(rs.getString("ename"));
                u.setJob(rs.getString("job"));
                u.setMgr(rs.getInt("mgr"));
                u.setHiredate(rs.getDate("hiredate"));
                u.setSal(rs.getDouble("sal"));
                u.setComm(rs.getDouble("comm"));
                u.setDeptno(rs.getInt("deptno"));
                userList.add(u);
            }
            System.out.println(userList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // 关闭Connection
    }
}
