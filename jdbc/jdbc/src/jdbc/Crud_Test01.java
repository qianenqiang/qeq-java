package jdbc;

import bean.User;
import org.junit.Test;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 使用jdbc实现增删改
 */
public class Crud_Test01 {
    @Test
    public void testInsert() throws ClassNotFoundException, SQLException {
       //注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //获取连接
        Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/study","root",
                "qianenqiang");
        //执行sql语句的对象statement
        Statement stmt=conn.createStatement();
        //执行sql语句，并得到返回结果
        int flag=stmt.executeUpdate("insert into salgrade values(3,4,5)");
        if(flag>0){
            System.out.println("成功");
        }
        //关闭资源
        stmt.close();
        conn.close();
    }
    @Test
    public void testUpdate() throws Exception{
        //注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //获取连接
        Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/study","root",
                "qianenqiang");
        //执行sql语句的对象statement
        Statement stmt=conn.createStatement();
        //执行sql语句，并得到返回结果
        int flag=stmt.executeUpdate("update salgrade set grade=6 where losal=5");
        if(flag>0){
            System.out.println("成功");
        }
        //关闭资源
        stmt.close();
        conn.close();
    }
    @Test
    public void testDelete() throws Exception{
        //注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //获取连接
        Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/study","root",
                "qianenqiang");
        //执行sql语句的对象statement
        Statement stmt=conn.createStatement();
        //执行sql语句，并得到返回结果
        int flag=stmt.executeUpdate("delete from salgrade where grade=6");
        if(flag>0){
            System.out.println("成功");
        }
        //关闭资源
        stmt.close();
        conn.close();
    }
    @Test
    public void testSelect() {
        Connection conn=null;
        Statement stmt=null;
        ResultSet rs=null;
        try {
            //注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //获取连接Connection
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/study", "root",
                    "qianenqiang");
            //得到执行sql语句的对象Statement
            stmt = conn.createStatement();
            //执行sql并返回结果
            rs = stmt.executeQuery("select empno,ename,job,mgr,hiredate,sal,comm,deptno from emp");
            //处理结果
            List<User> userList = new ArrayList<>();
            while (rs.next()) {
                User u = new User();
                u.setEmpno(rs.getInt("empno"));
                u.setEname(rs.getString("ename"));
                u.setJob(rs.getString("job"));
                u.setMgr(rs.getInt("mgr"));
                u.setHiredate(rs.getDate("hiredate"));
                u.setSal(rs.getDouble("sal"));
                u.setComm(rs.getDouble("comm"));
                u.setDeptno(rs.getInt("deptno"));
                userList.add(u);
            }
            System.out.println(userList);
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            try {
                // 关闭Connection
                if(rs!=null) {
                    rs.close();
                }
                if(stmt!=null){
                    stmt.close();
                }
                if(conn!=null){
                    conn.close();
                }
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
    }
}
