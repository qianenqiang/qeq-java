package jdbc;

import java.sql.*;

public class Jdbc_text{
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //获取连接Connection
        Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/study","root",
                "qianenqiang");
        //得到执行sql语句的对象Statement
        Statement stmt =conn.createStatement();
        //执行sql并返回结果
        ResultSet rs=stmt.executeQuery("select empno,job,sal from emp");
        //处理结果
        while(rs.next()){
            //System.out.println(rs.getObject("id"));
            System.out.println(rs.getObject("empno"));
            System.out.println(rs.getObject("job"));
            //System.out.println(rs.getObject("birthday"));
            System.out.println(rs.getObject("sal"));
        }
        // 关闭Connection
        rs.close();
        stmt.close();
        conn.close();
    }
}