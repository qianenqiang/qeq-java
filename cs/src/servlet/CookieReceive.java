package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/*
接受客户端请求中携带的Cookie
 */
public class CookieReceive extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            System.out.println("没有cookie");
        } else {
            for (Cookie c : cookies) {
                System.out.println("name" + c.getName());
                System.out.println("value" + c.getValue());
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
