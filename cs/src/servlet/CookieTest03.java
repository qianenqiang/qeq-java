package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/*
设置Cookie的失效时间
 */
public class CookieTest03 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        //创建一个Cookie对象，第一个参数类似map中的key,第二个类似map中的value，不要直接存中文
        Cookie cookie1=new Cookie("username","admin");
        Cookie cookie2=new Cookie("password","123456");
        //手动设置绑定路径
        cookie1.setPath(request.getContextPath()+"/aaa");
        cookie2.setPath(request.getContextPath()+"/aaa");
        //设置Cookie的失效时间，参数的单位是秒
        cookie1.setMaxAge(60*60);//一小时
        cookie2.setMaxAge(60*60*24);//一天
        //将cookie对象添加到响应中
        response.addCookie(cookie1);
        response.addCookie(cookie2);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
