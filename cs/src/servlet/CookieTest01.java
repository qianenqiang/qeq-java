package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/*
Cookie的默认路径
 */
public class CookieTest01 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
       //创建一个Cookie对象，第一个参数类似map中的key,第二个类似map中的value，不要直接存中文
        Cookie cookie1=new Cookie("username","admin");
        Cookie cookie2=new Cookie("password","123456");
        //将cookie对象添加到响应中
        response.addCookie(cookie1);
        response.addCookie(cookie2);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       doGet(request,response);
    }
}
