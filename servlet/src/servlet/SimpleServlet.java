package servlet;

import javax.servlet.*;
import java.io.IOException;
/*
  继承GenericSerclet类
 */
public class SimpleServlet extends GenericServlet {
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("SimpleServletji继承GenericServlet类");
    }

//    @Override
//    public void init(ServletConfig config) throws ServletException {
//        //果如忘记写该行代码时，可能会出现空指针错误
//        super.init(config);
//        System.out.println("SimpleServlet继承GenericServlet类");
//        //报出空指针异常
//        super.getServletName();
//    }
    //建议重写GenericServlet中的init方法
    @Override
    public void init() throws ServletException {
        System.out.println("hello");
    }
}
