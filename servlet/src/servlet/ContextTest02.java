package servlet;

import javax.servlet.*;

public class ContextTest02 implements Servlet {
    private ServletConfig config;

    @Override
    public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException {
        ServletContext application = this.config.getServletContext();
        System.out.println("Context02中的application:"+application);
        String admin= (String) application.getAttribute("admin");
        System.out.println(admin);
        String password=(String) application.getAttribute("password");
        System.out.println(password);
    }

    @Override
    public void destroy() {
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.config = config;
    }
}
