package servlet;

import javax.servlet.*;

public class LifeServlet implements Servlet {
    //注意在servlet不要定义可修改的成员变量 会有线程安全问题
    //private int count;
    public LifeServlet() {
        System.out.println("调用无参构造方法");
    }

    @Override
    public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException {
        //System.out.println(++count);
        System.out.println("调用service方法");
    }

    @Override
    public void destroy() {
        System.out.println("调用destroy方法");
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void init(ServletConfig arg0) throws ServletException {
        System.out.println("调用init方法");
    }
}
