package servlet;

import javax.servlet.*;

public class FirstServlet implements Servlet {
    @Override
    public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException {
        System.out.println("Hello");
    }

    @Override
    public void destroy() {
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void init(ServletConfig arg0) throws ServletException {
    }
}
