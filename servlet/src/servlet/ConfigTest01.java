package servlet;

import javax.servlet.*;
import java.util.Enumeration;

public class ConfigTest01 implements Servlet {
    private ServletConfig config;

    @Override
    public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException {
        //获取web.xml配置中的初始化参数
        String userName = config.getInitParameter("Username");
        System.out.println(userName);
        //获取当前servlet全部的初始化参数name
        Enumeration<String> params = config.getInitParameterNames();
        while (params.hasMoreElements()) {
            String name = params.nextElement();
            String value = config.getInitParameter(name);
            System.out.println(name + "=" + value);
        }
        //获取servlet的名称
        System.out.println("servletName" + config.getServletName());
    }

    @Override
    public void destroy() {
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.config = config;
    }
}
