package servlet;

import javax.servlet.*;

public class ContextTest01 implements Servlet {
    private ServletConfig config;
    @Override
    public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException {
        ServletContext application = this.config.getServletContext();
        System.out.println("Context02中的application:"+application);
        //获取应用的初始化数据
        String driver = application.getInitParameter("MySQLDriver");
        System.out.println(driver);
        //获取路径
        String contextPath = application.getContextPath();
        System.out.println("contextPath:" + contextPath);
        //获取class文件在硬盘中的绝对路径
        String realPath = application.getRealPath("FirstServlet");
        System.out.println(realPath);
        //向servletContext中添加属性
        application.setAttribute("admin", "tiger");
        application.setAttribute("password", "123456");
        //删除password
        application.removeAttribute("password");
    }

    @Override
    public void destroy() {
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.config = config;
    }
}
