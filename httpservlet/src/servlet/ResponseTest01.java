package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/*
使用HttpServletRequest接受请求注释
 */
public class ResponseTest01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置post请求的字符编码，此方式之对post请求有效
        request.setCharacterEncoding("UTF-8");
        //根据html中的name名字获取用户在input中填写的value值
        String username=request.getParameter("username");
        //注意：一定要在PrintWriter对象之前设置字符编码
        //方式一：
        //设置字符编码为UTF-8
        //response.setCharacterEncoding("UTF-8");
        //告诉浏览器以UTF-8的编码方式解码
        //response.addHeader("Context-type","text/html;charset=UTF-8");
        //解决响应乱码方式二,建议使用：
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out=response.getWriter();
        out.print("用户名："+username+"注册成功！<br>");
        out.print("感谢您的注册");
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
