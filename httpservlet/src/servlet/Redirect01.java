package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
public class Redirect01 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置字符编码
        request.setCharacterEncoding("UTF-8");
        //过去请求参数
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        request.setAttribute("username",username);
        request.setAttribute("password",password);
        //重定向
        //response.sendRedirect("Other");
        //跳转到另外一个web应用上的url(servlet)
        response.sendRedirect("/servlet/first");
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
