package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
使用HttpServletRequest接受请求注释
 */
public class RequestTest01 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置post请求字符的编码，此方式之对post请求有效
        request.setCharacterEncoding("UTF-8");
        //同时解决get和post乱码的方式
        //根据html中的name的名字获取用户在input中填写的值
        //代码量较大，实际应用中使用不多
//        String username=request.getParameter("username");
//        //将数组按照ISo885-1编码后放到字节数组中
//        byte[] bytes=username.getBytes("IOS8859-1");
//        //将数组按照UTF-8解码为字符串
//        username=new String(bytes,"UTF-8");
        //根据html中的名字获取用户在input中填写的value值
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        //获取用户勾选的checkbox的值
        String hobby[]=request.getParameterValues("hobby");
        System.out.println(username);
        System.out.println(password);
        for(String s:hobby){
            System.out.println(s);
        }
        //获取客户端IP地址
        String ip=request.getRemoteAddr();
        System.out.println(ip);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       doGet(request,response);
    }
}
